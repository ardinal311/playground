package id.gli.alfaswitch

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View

class AlfaVoucher : View {

    // Public Property
    var mBackgroundTop: Drawable? = null
        set(value) {
            field = value
            initElements()
        }

    var mBackgroundBottom: Drawable? = null
        set(value) {
            field = value
            initElements()
        }

    var mBackgroundColor: Int = 0
        set(value) {
            field = value
            initElements()
        }

    var mScallopRadius: Int = 0
        set(value) {
            field = value
            initElements()
        }

    var mScallopPositionPercent: Float = 0f
        set(value) {
            field = value
            initElements()
        }

    var mCornerRadius: Int = 0
        set(value) {
            field = value
            initElements()
        }

    var mShadowSize: Float = 0f
        set(value) {
            field = value
            setShadowBlurRadius(value)
            initElements()
        }

    var mShadowColor: Int = 0
        set(value) {
            field = value
            initElements()
        }

    // Private Property
    private var mDirty = true
    private var mPath = Path()
    private val mRect = RectF()

    /// Corner
    private var mRoundedCornerArc = RectF()

    /// Shadow
    private var mShadow: Bitmap? = null
    private var mShadowPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mShadowBlurRadius = 0f

    /// Scallop
    private var mScallopHeight = 0
    private var mScallopPosition = 0f

    /// Divider
    private var mDividerStart: Float = 0f
    private var mDividerStop: Float = 0f

    // Background
    private val mBackgroundPaint: Paint = Paint()

    constructor(
        context: Context?
    ) : super(context) {
        init(null)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (mDirty) doLayout()

        canvas?.let { c ->
            mShadow?.let {
                if (mShadowBlurRadius > 0f && !isInEditMode)
                    c.drawBitmap(it, 0f, mShadowBlurRadius / 2f, null)
            }

            c.drawPath(mPath, mBackgroundPaint)
            c.clipPath(mPath)

            mBackgroundTop?.let {
                setBackgroundTop(c)
            }
            mBackgroundBottom?.let {
                setBackroundBottom(c)
            }
        }
    }

    private fun doLayout() {
        val left = paddingLeft + mShadowBlurRadius
        val right = width - paddingRight - mShadowBlurRadius
        val top = paddingTop + (mShadowBlurRadius / 2)
        val bottom = height - paddingBottom - mShadowBlurRadius - (mShadowBlurRadius / 2)
        val offset = ((top + bottom) / mScallopPosition) - mScallopRadius

        mPath.reset()

        mPath.apply {
            arcTo(getTopLeftCornerRoundedArc(top, left), 180.0f, 90.0f, false)
            lineTo(left + mCornerRadius, top)
            lineTo(right - mCornerRadius, top)
            arcTo(getTopRightCornerRoundedArc(top, right), -90.0f, 90.0f, false)
        }

        mRect.set(
            right - mScallopRadius,
            top + offset,
            right + mScallopRadius,
            mScallopHeight + offset + top
        )

        mPath.apply {
            arcTo(mRect, 270f, -180.0f, false)
            arcTo(getBottomRightCornerRoundedArc(bottom, right), 0.0f, 90.0f, false)
            lineTo(right - mCornerRadius, bottom)
            lineTo(left + mCornerRadius, bottom)
            arcTo(getBottomLeftCornerRoundedArc(left, bottom), 90.0f, 90.0f, false)
        }

        mRect.set(
        left - mScallopRadius,
            top + offset,
            left + mScallopRadius,
            mScallopHeight + offset + top
        )

        mPath.arcTo(mRect, 90.0f, -180.0f, false)
        mPath.close()

        mDividerStart = mScallopRadius + top + offset
        mDividerStop = mScallopRadius + top + offset

        generateShadow()
        mDirty = false
    }

    private fun generateShadow() {
        if (isInEditMode) return
        if (mShadowBlurRadius == 0f) return

        mShadow.let {
            if (it == null) mShadow = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            else mShadow?.eraseColor(Color.TRANSPARENT)
        }

        val c = Canvas(mShadow!!)
        c.drawPath(mPath, mShadowPaint)

        val rs = RenderScript.create(context)
        val input = Allocation.createFromBitmap(rs, mShadow)
        val output = Allocation.createTyped(rs, input.type)
        val blur = ScriptIntrinsicBlur.create(rs, Element.U8(rs))

        blur.apply {
            setRadius(mShadowBlurRadius)
            setInput(input)
            forEach(output)
        }

        output.copyTo(mShadow)

        input.destroy()
        blur.destroy()
        output.destroy()
        rs.destroy()
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.AlfaVoucher)
            mBackgroundTop = a.getDrawable(
                R.styleable.AlfaVoucher_backgroundTop
            )
            mBackgroundBottom = a.getDrawable(
                R.styleable.AlfaVoucher_backgroundBottom
            )
            mBackgroundColor = a.getColor(
                R.styleable.AlfaVoucher_backgroundColor,
                Color.parseColor("#FFFFFF")
            )
            mScallopRadius = a.getDimensionPixelSize(
                R.styleable.AlfaVoucher_scallopRadius,
                dpToPxInt(16f)
            )
            mScallopPositionPercent = a.getFloat(
                R.styleable.AlfaVoucher_scallopPositionPercent,
                50f
            )
            mCornerRadius = a.getDimensionPixelSize(
                R.styleable.AlfaVoucher_cornerRadius,
                dpToPxInt(8f)
            )

            when {
                a.hasValue(R.styleable.AlfaVoucher_shadowSize) -> {
                    mShadowSize = a.getDimension(
                        R.styleable.AlfaVoucher_shadowSize,
                        mShadowSize
                    )
                }
                a.hasValue(R.styleable.AlfaVoucher_android_elevation) -> {
                    mShadowSize = a.getDimension(
                        R.styleable.AlfaVoucher_android_elevation,
                        mShadowSize
                    )
                }
            }

            if (mShadowSize > 0f) {
                setShadowBlurRadius(mShadowSize)
            }

            mShadowColor = a.getColor(
                R.styleable.AlfaVoucher_shadowColor,
                Color.parseColor("#000000")
            )

            a.recycle()
        }

        initElements()
        setLayerType(LAYER_TYPE_SOFTWARE, null)
    }

    private fun initElements() {
        mScallopPosition = 100 / mScallopPositionPercent
        mScallopHeight = mScallopRadius * 2

        setShadowPaint()
        setBackgroundPaint()

        mDirty = true
        invalidate()
    }

    private fun setBackgroundTop(c: Canvas) {
        val left = paddingLeft + mShadowBlurRadius
        val top = paddingTop + (mShadowBlurRadius / 2)
        val right = width - paddingRight - mShadowBlurRadius

        mBackgroundTop?.let {
            it.setBounds(left.toInt(), top.toInt(), right.toInt(), mDividerStart.toInt())
            it.draw(c)
        }
    }

    private fun setBackroundBottom(c: Canvas) {
        val left = paddingLeft + mShadowBlurRadius
        val right = width - paddingRight - mShadowBlurRadius
        val bottom = height - paddingBottom - mShadowBlurRadius - (mShadowBlurRadius / 2)

        mBackgroundBottom?.let {
            it.setBounds(left.toInt(), mDividerStop.toInt(), right.toInt(), bottom.toInt())
            it.draw(c)
        }
    }

    private fun setShadowPaint() {
        mShadowPaint.apply {
            colorFilter = PorterDuffColorFilter(mShadowColor, PorterDuff.Mode.SRC_IN)
            alpha = 20
        }
    }

    private fun setBackgroundPaint() {
        mBackgroundPaint.apply {
            alpha = 0
            isAntiAlias = true
            color = mBackgroundColor
            style = Paint.Style.FILL
        }
    }

    private fun getTopLeftCornerRoundedArc(top: Float, left: Float): RectF {
        mRoundedCornerArc.set(left, top, left + mCornerRadius * 2, top + mCornerRadius * 2)
        return mRoundedCornerArc
    }

    private fun getTopRightCornerRoundedArc(top: Float, right: Float): RectF {
        mRoundedCornerArc.set(right - mCornerRadius * 2, top, right, top + mCornerRadius * 2)
        return mRoundedCornerArc
    }

    private fun getBottomRightCornerRoundedArc(bottom: Float, right: Float): RectF {
        mRoundedCornerArc.set(right - mCornerRadius * 2, bottom - mCornerRadius * 2, right, bottom)
        return mRoundedCornerArc
    }

    private fun getBottomLeftCornerRoundedArc(left: Float, bottom: Float): RectF {
        mRoundedCornerArc.set(left, bottom - mCornerRadius * 2, left + mCornerRadius * 2, bottom)
        return mRoundedCornerArc
    }

    // utils
    private fun setShadowBlurRadius(elevation: Float) {
        val maxElevation: Float = dpToPxFloat(24f)
        mShadowBlurRadius = (25f * (elevation / maxElevation)).coerceAtMost(25f)
    }

    private fun dpToPxInt(dp: Float): Int {
        return dpToPxFloat(dp).toInt()
    }

    private fun dpToPxFloat(dp: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
    }
}