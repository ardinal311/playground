package id.gli.alfaswitch

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.widget.LinearLayout

class AlfaSwitch : LinearLayout {

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    var trackBackground: Drawable? = null
        set(value) {
            field = value
            invalidate()
        }

    var thumbBackground: Drawable? = null
        set(value) {
            field = value
            invalidate()
        }

    private fun init(attrs: AttributeSet?) {
        orientation = HORIZONTAL
        isBaselineAligned = false
        setBackgroundColor(Color.parseColor("565656"))
    }

    private fun getAttributes(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.AlfaSwitch)
        thumbBackground = a.getDrawable(R.styleable.AlfaSwitch_thumbDrawable)
        trackBackground = a.getDrawable(R.styleable.AlfaSwitch_trackDrawable)
        a.recycle()
    }

    override fun setBackground(background: Drawable?) {
        // no implementation
    }
}